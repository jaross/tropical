package com.krivea.tropical;

import org.elasticsearch.action.admin.cluster.stats.ClusterStatsResponse;
import org.elasticsearch.action.admin.indices.get.GetIndexResponse;
import org.elasticsearch.client.Client;
import org.elasticsearch.common.base.Function;
import org.elasticsearch.common.collect.Collections2;
import org.elasticsearch.common.collect.Lists;
import org.elasticsearch.common.hppc.cursors.ObjectObjectCursor;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.node.Node;
import org.elasticsearch.node.NodeBuilder;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.PostConstruct;
import java.util.Collection;

import static org.elasticsearch.client.Requests.clusterStatsRequest;

@Controller
@EnableAutoConfiguration
public class SampleController {

  private Client client;

  @PostConstruct
  public void init() {
    Node node = NodeBuilder.nodeBuilder().client(true).node();
    client = node.client();
  }

  @RequestMapping("/")
  @ResponseBody
  String home() {
    return "Hello World!";
  }


  @RequestMapping("/cluster")
  @ResponseBody
  public ClusterStats indexes() {
    ClusterStatsResponse response = client.admin().cluster().clusterStats(clusterStatsRequest()).actionGet();
    response.getIndicesStats().getStore().getSizeInBytes();
    return new ClusterStats(response.getClusterNameAsString(),
        response.getStatus()
    );
  }

  @RequestMapping("/indexes")
  @ResponseBody
  public Collection<IndexSettings> indexByName() {
    GetIndexResponse indexResponse = client.admin().indices().prepareGetIndex().execute().actionGet();
    return Collections2.transform(Lists.newArrayList(indexResponse.settings()), new Function<ObjectObjectCursor<String, Settings>, IndexSettings>() {
      @Override
      public IndexSettings apply(ObjectObjectCursor<String, Settings> settings) {
        return new IndexSettings(settings.key, settings.value.getAsMap());
      }
    });
  }


  public static void main(String[] args) throws Exception {
    SpringApplication.run(SampleController.class, args);
  }


}
