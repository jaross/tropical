package com.krivea.tropical;

import org.elasticsearch.common.collect.ImmutableMap;

import java.util.Map;

public class IndexSettings {
  private final String name;
  private final Map<String, String> settings;

  public IndexSettings(String name, ImmutableMap<String, String> settings) {
    this.name = name;
    this.settings = settings;
  }

  public String getName() {
    return name;
  }

  public Map<String, String> getSettings() {
    return settings;
  }
}
