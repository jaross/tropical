package com.krivea.tropical;

import org.elasticsearch.action.admin.cluster.health.ClusterHealthStatus;

public class ClusterStats {
  private final String clusterName;
  private final ClusterHealthStatus status;

  public ClusterStats(String clusterName, ClusterHealthStatus status) {
    this.clusterName = clusterName;
    this.status = status;
  }

  public String getClusterName() {
    return clusterName;
  }

  public ClusterHealthStatus getStatus() {
    return status;
  }
}
